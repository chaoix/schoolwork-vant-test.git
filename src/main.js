import { createApp } from 'vue'

import App from './components/pages/test.vue'
import AntDesignVue from 'ant-design-vue'
import Vant from 'vant'


const app = createApp(App)
app.use(Vant)
//app.use(AntDesignVue)

app.mount('#app')
